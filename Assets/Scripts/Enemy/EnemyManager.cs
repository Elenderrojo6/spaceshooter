﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public float timeEnemy;
    private float currentTime = 0;
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeEnemy){
            currentTime = 0;
            Instantiate(EnemyPrefab, new Vector3(10,Random.Range(12,-12),0),Quaternion.identity,this.transform);
        }
    }
}
