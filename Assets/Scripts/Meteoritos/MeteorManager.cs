﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
    public GameObject meteorPrefab;
    public float timeLaunchMeteor;
    private float currentTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLaunchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab, new Vector3(10,Random.Range(12,-12),0),Quaternion.identity,this.transform);
        }
    }
}
