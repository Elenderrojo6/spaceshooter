﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeaponVertical : Weapon
{

    public GameObject laserBulletDown;
    public GameObject laserBullet;
    public GameObject laserBulletUp;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (laserBulletDown, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBulletUp, this.transform.position, Quaternion.identity, null);
        Instantiate (laserBullet, this.transform.position, Quaternion.identity, null);
    }

}
